#include <cstddef>
#include <utility>

#pragma once

template <typename... Args>
class tuple;

template <>
class tuple<> {
};

template <typename First, typename... Args>
class tuple<First, Args...> : tuple<Args...> {
  public:
    tuple() = default;

    template <typename F, typename... As,
              typename = std::enable_if_t<std::is_constructible_v<First, F>>>
    constexpr explicit tuple(F &&f, As &&... args)
        : tuple<Args...>{std::forward<As>(args)...}, value(std::forward<F>(f)) {}

  private:
    template <std::size_t, typename, typename...>
    friend struct get_index_impl;

    template <typename, typename, typename...>
    friend struct get_type_impl;

    First value;
};

template <std::size_t I, typename First, typename... Args>
struct get_index_impl {
    static constexpr auto const &
    get_value(tuple<First, Args...> const &t) {
        return get_index_impl<I - 1, Args...>::get_value(t);
    }

    static constexpr auto &
    get_value(tuple<First, Args...> &t) {
        return get_index_impl<I - 1, Args...>::get_value(t);
    }
};

template <typename First, typename... Args>
struct get_index_impl<0, First, Args...> {
    static constexpr auto const &
    get_value(tuple<First, Args...> const &t) {
        return t.value;
    }

    static constexpr auto &
    get_value(tuple<First, Args...> &t) {
        return t.value;
    }
};

template <typename T, typename First, typename... Args>
struct get_type_impl {
    static constexpr T const &
    get_value(tuple<First, Args...> const &t) {
        return get_type_impl<T, Args...>::get_value(t);
    }

    static constexpr T &
    get_value(tuple<First, Args...> &t) {
        return get_type_impl<T, Args...>::get_value(t);
    }
};

template <typename T, typename... Args>
struct get_type_impl<T, T, Args...> {
    static constexpr T const &
    get_value(tuple<T, Args...> const &t) {
        return t.value;
    }

    static constexpr T &
    get_value(tuple<T, Args...> &t) {
        return t.value;
    }
};

template <typename T, typename... Args>
struct Count;

template <typename T>
struct Count<T> {
    static constexpr std::size_t value = 0;
};

template <typename T, typename First, typename... Args>
struct Count<T, First, Args...> {
    static constexpr std::size_t value = std::is_same_v<T, First> + Count<T, Args...>::value;
};

template <std::size_t I, typename... Args>
constexpr auto const &
get(tuple<Args...> const &t) {
    return get_index_impl<I, Args...>::get_value(t);
}

template <std::size_t I, typename... Args>
constexpr auto &
get(tuple<Args...> &t) {
    return get_index_impl<I, Args...>::get_value(t);
}

template <typename T, typename... Args>
constexpr std::enable_if_t<Count<T, Args...>::value == 1, T const &>
get(tuple<Args...> const &t) {
    return get_type_impl<T, Args...>::get_value(t);
}

template <typename T, typename... Args>
constexpr std::enable_if_t<Count<T, Args...>::value == 1, T &>
get(tuple<Args...> &t) {
    return get_type_impl<T, Args...>::get_value(t);
}
